# cadvisor2cloudwatch - Sending docker metrics from cAdvisor to AWS Cloudwatch

cadvisor2cloudwatch is a small program that export docker metrics from cAdvisor to AWS Cloudwatch. By running it as a cron (every minute), you can send the metrics from cAdvisor (memory and CPU usage for the moment) to AWS Cloudwatch as custom metrics, and consult them directly in the AWS Console.

A example of use case would be to deploy cAdvisor and cadvisor2cloudwatch on each ECS instances to get metrics from the docker containers, which are not provided by Cloudwatch by default.

## Building
`go build cadvisor2cloudwatch.go`

## Running
`./cadvisor2cloudwatch [options] CADVISOR_ENDPOINT`, with options among:

* -access-key="": AWS access key
* -profile="": AWS profile
* -region="": AWS region where the metrics will be put
* -secret-key="": AWS secret key
* -cron=false: run the program every minutes

And CADVISOR_ENDPOINT being for example `http://localhost:8080/`.

If you do not use one of the options above, values will be taken from the environment variables, credentials file or role as explained [here](https://github.com/aws/aws-sdk-go/wiki/Getting-Started-Configuration).

## TODO
* Dockerfile for building
* Dockerfile for running
* Documentation
* Adding/improving metrics
* Using goroutines to upload in parrallel
* Adding underlying machine id to metrics dimensions
* Clean, comment and rename
