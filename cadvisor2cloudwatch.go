package main

import (
	"flag"
	"log"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/defaults"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
	"github.com/google/cadvisor/client"
	info "github.com/google/cadvisor/info/v1"
)

var now time.Time = time.Now()

func main() {
	region := flag.String("region", "", "AWS region where the metrics will be put")
	accessKey := flag.String("access-key", "", "AWS access key")
	secretKey := flag.String("secret-key", "", "AWS secret key")
	profile := flag.String("profile", "", "AWS profile")
	cron := flag.Bool("cron", false, "Run the program every minutes")
	flag.Parse()
	globalAWSConfiguration(region, profile, accessKey, secretKey)
	endpoint := flag.Arg(0)
	if endpoint == "" {
		log.Fatal("You must provide an endpoint!")
	}
	if *cron {
		log.Println("Launching cadvisor2cloudwatch in cron mode...")
		now = <-time.After(1*time.Minute - time.Duration(now.Second())*time.Second - time.Duration(now.Nanosecond())*time.Nanosecond)
		go getAndPutMetrics(endpoint)
		for now = range time.Tick(time.Minute) {
			go getAndPutMetrics(endpoint)
		}

	} else {
		log.Println("Launching cadvisor2cloudwatch...")
		if err := getAndPutMetrics(endpoint); err != nil {
			log.Fatal("Execution ended with errors")
		}
		log.Println("Execution finished with success")
	}
}

func getAndPutMetrics(endpoint string) error {
	sInfo, err := getContainersInfo(endpoint)
	if err != nil {
		log.Println(err.Error())
	}
	if err := pushContainersMetricStatsToCloudwatch(sInfo); err != nil {
		if awsErr, ok := err.(awserr.Error); ok {
			// Generic AWS error with Code, Message, and original error (if any)
			log.Println(awsErr.Code(), awsErr.Message(), awsErr.OrigErr())
			if reqErr, ok := err.(awserr.RequestFailure); ok {
				// A service error occurred
				log.Println(reqErr.Code(), reqErr.Message(), reqErr.StatusCode(), reqErr.RequestID())
			}
		} else {
			// This case should never be hit, the SDK should always return an
			// error which satisfies the awserr.Error interface.
			log.Println(err.Error())
		}
		return err
	}
	return nil
}

func globalAWSConfiguration(region, profile, accessKey, secretKey *string) {
	if *region != "" {
		defaults.DefaultConfig.Region = region
	}
	if *profile != "" {
		defaults.DefaultConfig.Credentials = credentials.NewSharedCredentials("", *profile)
	} else if *accessKey != "" && *secretKey != "" {
		defaults.DefaultConfig.Credentials = credentials.NewStaticCredentials(*accessKey, *secretKey, "")
	}
}

func getContainersInfo(endpoint string) ([]info.ContainerInfo, error) {
	c, err := client.NewClient(endpoint)
	if err != nil {
		return nil, err
	}
	request := lastMinuteContainerInfoRequest()
	return c.SubcontainersInfo("/docker", request)
}

func containerStatsToCWStats(cStats []*info.ContainerStats, cName, cId string) []*cloudwatch.MetricDatum {
	memorySet := new(cloudwatch.StatisticSet)
	cpuSet := new(cloudwatch.StatisticSet)
	for _, v := range cStats {
		processMetricLoop(float64(v.Memory.Usage), memorySet)
		processMetricLoop(float64(v.Cpu.Usage.Total), cpuSet)
	}
	return []*cloudwatch.MetricDatum{metricDatumFromStatSet("MemoryUsage", memorySet, cName, cId), metricDatumFromStatSet("CpuUsage", cpuSet, cName, cId)}
}
func metricDatumFromStatSet(metricName string, statSet *cloudwatch.StatisticSet, cName, cId string) *cloudwatch.MetricDatum {
	return &cloudwatch.MetricDatum{
		MetricName:      aws.String(metricName),
		StatisticValues: statSet,
		Timestamp:       aws.Time(now),
		Dimensions:      []*cloudwatch.Dimension{{Name: aws.String("cName"), Value: &cName}, {Name: aws.String("cId"), Value: &cId}},
	}
}
func processMetricLoop(metric float64, statSet *cloudwatch.StatisticSet) {
	if statSet.SampleCount == nil {
		statSet.Minimum, statSet.Maximum = aws.Float64(metric), aws.Float64(metric)
		statSet.SampleCount = aws.Float64(1)
		statSet.Sum = aws.Float64(metric)
	} else {
		if metric > *statSet.Maximum {
			*statSet.Maximum = metric
		}
		if metric < *statSet.Minimum {
			*statSet.Minimum = metric
		}
		*statSet.Sum += metric
		*statSet.SampleCount++
	}
}

func pushContainersMetricStatsToCloudwatch(cInfo []info.ContainerInfo) error {
	for _, v := range cInfo {
		if v.Name == "/docker" {
			continue
		}
		log.Println("Processing", v.Aliases, "...")
		err := pushContainerMetricStatsToCloudwatch(&v)
		if err != nil {
			return err
		}
		log.Println(v.Aliases, "processed with success!")
	}
	log.Println("All containers processed with success!")
	return nil
}

func pushContainerMetricStatsToCloudwatch(cInfo *info.ContainerInfo) error {
	stats := containerStatsToCWStats(cInfo.Stats, cInfo.Aliases[0], cInfo.Aliases[1])
	request := cloudwatch.PutMetricDataInput{Namespace: aws.String("docker"), MetricData: stats}
	log.Println("Sending request:", request)
	svc := cloudwatch.New(nil)
	_, err := svc.PutMetricData(&request)
	return err
}

func lastMinuteContainerInfoRequest() *info.ContainerInfoRequest {
	oneMinuteAgo := now.Add((-1) * time.Minute)
	return &info.ContainerInfoRequest{NumStats: -1, Start: oneMinuteAgo, End: now}
}
